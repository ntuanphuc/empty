#run every 10 minutes to pull code from repos

cd /usr/share/nginx/html/tplus/insurance-dev/

gitResponse="$(git pull origin develop 2>&1)"

newCode="Updating"
upToDate="Already up-to-date."
composerChanged="composer.json"
#conflictCode="error: Your local changes to the following files would be overwritten by merge:"
conflictCode="error:"


if echo "$gitResponse" | grep -q "$conflictCode"; then
  echo "Conflict"
fi

if echo "$gitResponse" | grep -q "$upToDate"; then
  echo "Aloha, all up-to-date.";
else
  #this may conflict or new code found and can be ready to run
  #check conflict
  if echo "$gitResponse" | grep -q "$conflictCode"; then
    echo "Code conflict, please check and re-update"
  else
    if echo "$gitResponse" | grep -q "$composerChanged"; then
      #runComposer="$(composer update)"
      composerResponse="$(composer update 2>&1)"
      composerSuccess="Generating autoload files"
      if echo "$composerResponse" | grep -q "$composerSuccess"; then
        echo "Composer done";
      else
        echo "Composer error";
      fi
    else
      echo "New code found, ready to run"
    fi
  fi
fi